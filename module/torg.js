/**
 * Adaptation of the "simple wold building" of Atropos to handle collections of character and item attributes
 * Author: cmorvan
 * Software License: GNU GPLv3
 */

// Import Modules
import { TORG } from "./config.js";
import { TorgActor } from "./actor.js";
import { SimpleItemSheet } from "./item-sheet.js";
import { TorgPcActorSheet } from "./actor-sheet.js";
import { TorgNPcActorSheet } from "./actor-sheet.js";

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function() {
  console.log(`Initializing Torg eternity System`);

	// Define custom Entity classes
  CONFIG.Actor.entityClass = TorgActor;

  // Register Basic Pc SHeet
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("torg", TorgPcActorSheet, { makeDefault: true });

  // Registering basic NPC sheet
  Actors.registerSheet("torg", TorgNPcActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("torg", SimpleItemSheet, {makeDefault: true});

  // Register system settings
  game.settings.register("torg-eternity", "macroShorthand", {
    name: "Shortened Macro Syntax",
    hint: "Enable a shortened macro syntax which allows referencing attributes directly, for example @str instead of @attributes.str.value. Disable this setting if you need the ability to reference the full attribute model, for example @attributes.str.label.",
    scope: "world",
    type: Boolean,
    default: true,
    config: false
  });
});
