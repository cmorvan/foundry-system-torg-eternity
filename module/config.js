/***
 * Various general purpose variable for Torg Eternity system.
 * 
 */

export const TORG = {
    /** The FATE Ladder */
    cosms: {
        "aysle":{
            "label": "Aysle"
            },
        "livingland":{
            "label": "Living Land"
            }
    },
    axioms: ["magic","social","spirit","tech"]
};
