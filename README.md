# Torg Eternity

This is a tentative to implement Torg eternity rules for Foundry vtt. There are several key aspects that we will try to implement progressively:
    1. Dice rolling and possibilities
    2. Card system (Cosm, Drama and Destiny)

Once this is done we will be able to move forward.

## Credits

This system has been built starting from the "worldbuilding" system by Atropos. 